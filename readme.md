# Microservi�o de Aluguel

Microservi�o respons�vel pelas opera��es de aluguel. Esse microservi�o possui duas rotas `HTTP`, sendo elas:

## Get Ciclista (/ciclista):
Opera��o `GET` que pega um ciclista baseado no id.

### C�digo HTTP:

Esse m�todo possui três c�digos de retorno poss�veis, sendo eles:

	1. 200: search results match criteria
	2. 404: not found
    3. 422: bad input parameters

## Post Ciclista (/ciclista):
Opera��o `POST` que cadastrada um novo ciclista no sistema. Recebe um objeto do tipo:

```
	{
  "id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
  "email": "joaosilva@email.com",
  "nome": "Jo�o Silva",
  "senha": "Joao1234!",
  "indicadorBr": true,
  "usandoBicicleta": true,
  "bicicletaAtiva": {
    "id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
    "codigo": 90851234678,
    "emUso": false
  },
  "documento": {
    "tipo": "CPF",
    "numero": 15954068715,
    "foto": "foto.jpg"
  },
  "cart�o": {
    "numero": 1234567890123456,
    "proprietario": "JOAO SILVA",
    "validade": "01/20",
    "codigo": 123
  }
}
```

### C�digo HTTP:

Esse m�todo possui dois c�digos de retorno poss�veis, sendo eles:

	1. 201: item created
	2. 405: item not created
