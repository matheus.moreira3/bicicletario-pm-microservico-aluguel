package com.unirio.pm.services;

import com.unirio.pm.domain.Ciclista;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class DBOServiceTest {

    @Test
    public void deveCriarCiclistaPorId() {
        Ciclista ciclista = Ciclista.builder().id("1").build();
        DBOService.createCiclista(ciclista);
        Ciclista ciclistaGet = (Ciclista)DBOService.getCiclistas().get(0);
        assertEquals(ciclista.getId(), ciclistaGet.getId());
    }

    @Test
    public void deveRemoverCiclistaPorId() {
        Ciclista ciclista = Ciclista.builder().id("1").build();
        DBOService.createCiclista(ciclista);
        assertEquals(true, DBOService.deleteCiclista("1"));
        assertEquals(true, DBOService.getCiclistas().isEmpty());
    }
}
