package com.unirio.pm.controllers;

import com.unirio.pm.domain.Ciclista;
import controllers.CiclistaController;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CiclistaControllerTest {
	private CiclistaController ciclistaController;

	@Before
	public void setup() {
		this.ciclistaController = new CiclistaController();
	}

	@Test
	public void deveCriarNovoCiclista() {
		this.ciclistaController.createCiclista(Ciclista.builder().id("1").build());
		assertEquals(1, this.ciclistaController.getAllCiclistas().size());
	}

	@Test
	public void deveDeletarCiclista() {
		this.ciclistaController.createCiclista(Ciclista.builder().id("1").build());
		boolean ciclistaDeletado = this.ciclistaController.deleteCiclista("1");
		assertEquals(true, ciclistaDeletado);
		assertEquals(null, this.ciclistaController.getCiclistaById(""));
	}

	@Test
	public void deveDarErroAoDeletarCiclista() {
		try {
			this.ciclistaController.createCiclista(Ciclista.builder().build());
			boolean ciclistaDeletado = this.ciclistaController.deleteCiclista("1");
		} catch (Exception e) {
			assertEquals("class java.lang.NullPointerException", e.getClass().toString());
		}
	}
	
}
