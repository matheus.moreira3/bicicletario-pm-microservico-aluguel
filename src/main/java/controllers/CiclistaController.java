package controllers;

import com.unirio.pm.domain.Ciclista;

import java.util.ArrayList;
import java.util.List;

public class CiclistaController {
	private List<Ciclista> ciclistas = new ArrayList<>();
	private static CiclistaController ciclistaController;
	
	public static CiclistaController getCiclistaController() {
		if (ciclistaController == null) {
			ciclistaController = new CiclistaController();
		}
		return ciclistaController;
	}

	public int getQuantityCiclistas() {
		return this.ciclistas.size();
	}

	public List<Ciclista> getAllCiclistas() {
		return this.ciclistas;
	}

	public Ciclista getCiclistaById(String id) {
		for(int i=0; i <= this.ciclistas.size() - 1; i++) {
			if (this.ciclistas.get(i).getId().equals(id)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}

	public Ciclista getCiclistaByName(String nome){
		for(int i=0; i <= this.ciclistas.size() - 1; i++) {
			if(this.ciclistas.get(i).getNome().equals(nome)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}

	public Ciclista getCiclistaByEmail(String email) {
		for(int i=0; i <= this.ciclistas.size() - 1; i++) {
			if (this.ciclistas.get(i).getEmail().equals(email)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}

	public boolean createCiclista(Ciclista c) {
		if (this.getCiclistaById(c.getId()) == null) {
			return this.ciclistas.add(c);
		} else {
			return false;
		}
	}

	public boolean deleteCiclista(String id) {
		return this.ciclistas.remove(this.getCiclistaById(id));
	}

}
