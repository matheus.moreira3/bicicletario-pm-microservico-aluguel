package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Ciclista {
    @JsonProperty("id")
    private String id;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("nascimento")
    private String nascimento;

    @JsonProperty("cpf")
    private String cpf;

    @JsonProperty("email")
    private String email;

    @JsonProperty("status")
    private CiclistaStatus status;

    @JsonProperty("senha")
    private String senha;

    @JsonProperty("nacionalidade")
    private Nacionalidade nacionalidade;

    @JsonProperty("indicadorBr")
    private boolean indicadorBr;

    @JsonProperty("usandoBicicleta")
    private boolean usandoBicicleta;

    @JsonProperty("cartao")
    private Cartao cartao;

    @JsonProperty("passaporte")
    private Passaporte passaporte;
}
