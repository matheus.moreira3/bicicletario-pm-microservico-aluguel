package com.unirio.pm.domain;

import lombok.Getter;

@Getter
public enum Nacionalidade {
    BRASILEIRO("BRASILEIRO", 1),
    ESTRANGEIRO("ESTRANGEIRO", 2);

    private String tipoNacionalidade;
    private Integer codigoNacionalidade;

    Nacionalidade(String tipoNacionalidade, Integer codigoNacionalidade) {
        this.tipoNacionalidade = tipoNacionalidade;
        this.codigoNacionalidade = codigoNacionalidade;
    }
}
