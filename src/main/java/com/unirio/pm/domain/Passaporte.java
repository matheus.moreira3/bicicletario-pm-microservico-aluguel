package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Passaporte {
    @JsonProperty("numero")
    private String numero;

    @JsonProperty("validade")
    private String validade;

    @JsonProperty("pais")
    private String pais;
}
