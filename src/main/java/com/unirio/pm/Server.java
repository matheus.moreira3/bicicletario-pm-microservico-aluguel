package com.unirio.pm;

import com.unirio.pm.domain.Ciclista;
import com.unirio.pm.domain.Error;
import com.unirio.pm.services.DBOService;
import io.javalin.Javalin;


public class Server {
	private final static String PATH = "/ciclista";
	private final static String NOT_FOUND_MESSAGE = "Não encontrado";
	private final static String INVALID_DATA_MESSAGE = "Dados Inválidos";

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}

	public static void main (String[] args) {
		Javalin app = Javalin.create().start(getHerokuAssignedPort());
        app.get(PATH, ctx -> {
        	String queryParam = ctx.queryParam("id");
			if (queryParam == null) {
				ctx.status(422).json(Error.builder().codigo("422").mensagem(INVALID_DATA_MESSAGE));
				return;
			}
			Ciclista ciclista = DBOService.getCiclista(queryParam);
			if (ciclista == null) {
				ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE));
				return;
			}
			ctx.status(200).json(ciclista);
		});
        app.post(PATH, ctx -> {
        	Ciclista ciclista = ctx.bodyAsClass(Ciclista.class);
        	if (ciclista == null) {
        		ctx.status(405).json(Error.builder().codigo("405").mensagem(INVALID_DATA_MESSAGE));
        	}
        	if (DBOService.createCiclista(ciclista)) {
        		ctx.status(201).json(ciclista);
        	} else {
				ctx.status(405).json(Error.builder().codigo("405").mensagem(INVALID_DATA_MESSAGE));
        	}
        });
	}
}
