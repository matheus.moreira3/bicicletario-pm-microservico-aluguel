package com.unirio.pm.services;

import java.util.List;

import com.unirio.pm.domain.Ciclista;
import controllers.CiclistaController;

public class DBOService {

	private DBOService() {}
	
	public static Ciclista getCiclista(String queryParam) {
		return CiclistaController.getCiclistaController().getCiclistaById(queryParam);
	}
	
	public static List<Ciclista> getCiclistas() {
		return CiclistaController.getCiclistaController().getAllCiclistas();
	}

	public static boolean deleteCiclista(String id) {
		if (id == null) { return false; }
		return CiclistaController.getCiclistaController().deleteCiclista(id);
	}

	public static boolean createCiclista(Ciclista ciclista) {
		return CiclistaController.getCiclistaController().createCiclista(ciclista);
	}
}
